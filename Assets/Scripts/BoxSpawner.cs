﻿using UnityEngine;

public class BoxSpawner : MonoBehaviour
{
    [SerializeField] private EnemyBoxesPool pool;
    [SerializeField] private BoxesSprites sprites;
    [SerializeField] private Vector2 horizontal;
    [SerializeField] private Vector2 vertical;
    
    private void Start()
    {
        /*vertical.x = (Screen.height / 2f) * -1;
        vertical.y = (Screen.height / 1.5f);
        horizontal.y = (Screen.width / 2) * (1 - offset);
        horizontal.x = horizontal.y * -1;*/
        //TODO: Adaptive
    }
    
    //in random place
    public void SpawnEnemyBox()
    {
        var box = TakeEnemyBox();
        var bt = SetStartPos(box.transform);
        
        var ePos = new Vector3(bt.x, vertical.x ,bt.z);
        box.StartFalling(bt, ePos);
    }

    public FallingBoxEnemy TakeEnemyBox()
    {
        var box = pool.GetEnemyBox();
        box.ChangeSprite(sprites.GetRandomEnemyBoxSprite());
        return box;
    }

    public Vector3 SetStartPos(Transform box)
    {
        float rX = UnityEngine.Random.Range(horizontal.x, horizontal.y);
        var sPos = new Vector3(rX,vertical.y, box.transform.position.z);
        box.transform.position = sPos;
        return box.transform.position;
    }

    public void ChangeSpeed(float nSpeed) => pool.ChangeFallingSpeed(nSpeed); 
    public int GetPoolSize() => pool.GetPoolSize();
    
    public Vector2 GetOffset()
    {
        return horizontal;
    }

   
}
