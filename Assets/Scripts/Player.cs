﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Vector2 current = new Vector2(0,0);
    [SerializeField] public float speed;
    
    private Vector2 road;
    private Vector2 idle = new Vector2(0, 0);
    private Vector2 left = new Vector2(-1, 0);
    private Vector2 right = new Vector2(1, 0);
    
    public Action<Collider2D> touch;
    
    public void Update()
    {
        GetTouchSide();
    }

    public void FixedUpdate()
    {
        transform.Translate(current * speed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        touch?.Invoke(other);
    }
    
    public void SetNewRoad(Vector2 r)
    {
        road = r;
    }
    
    private void GetTouchSide()
    {
        var tp = transform.position;
        if (Input.GetMouseButton(0))
        {
            var tempPos = Input.mousePosition;
            var pos = Camera.main.ScreenToWorldPoint(tempPos);
            
            if (pos.x > 0 && tp.x < road.y)
                current = right;
            else 
            if (pos.x < 0 && tp.x > road.x)
                current = left;
            else
                current = idle;
        }
        else
            current = idle;
    }
}
