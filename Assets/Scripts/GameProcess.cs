﻿using System.Collections;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameProcess : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private BoxSpawner spawner;
    [SerializeField] private Line line;
    
    [SerializeField] private GameObject UI;
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private Button button;
    
    [SerializeField] private float enemySpeedUpDelay;
    [SerializeField] private float enemySpeedUpStep;
    [SerializeField] private float enemyStartSpeed;
    
    
    private float currentEnemySpeed;
    private FallingBoxEnemy lastEnemy;
    WaitForSeconds enemySpeedUpDelayW = new WaitForSeconds(1);
    private int spawnedEnemyIter = 0;
    private void Start()
    {
        PlayerSettings.defaultInterfaceOrientation = UIOrientation.Portrait;
        enemySpeedUpDelayW = new WaitForSeconds(enemySpeedUpDelay);

        currentEnemySpeed = enemyStartSpeed;
        spawner.ChangeSpeed(enemyStartSpeed);

        StartCoroutine(ChangeSpeed());
        
        player.SetNewRoad(spawner.GetOffset());
        line.touch += LineTouch;
        player.touch += PlayerTouch;
        spawner.SpawnEnemyBox();
    }

    private void LineTouch(Collider2D a)
    {
        if (a.CompareTag("EnemyBox"))
        {
            spawner.SpawnEnemyBox();
            spawnedEnemyIter++;
        }
    }

    private void PlayerTouch(Collider2D a)
    {
        if (!a.CompareTag("EnemyBox"))
            return;

        StartCoroutine(EndGame());
    }

    private IEnumerator EndGame()
    {
        StopAllCoroutines();
        Destroy(player.gameObject);
        //Place fo ANim?
        UI.gameObject.SetActive(true);
        text.text = $"Score: {spawnedEnemyIter - spawner.GetPoolSize() + 1}";

        this.gameObject.SetActive(false);
        yield return null;
    }
    
    private IEnumerator ChangeSpeed()
    {
        while (true)
        {
            yield return enemySpeedUpDelayW;
            currentEnemySpeed += enemySpeedUpStep;
            spawner.ChangeSpeed(currentEnemySpeed);
        }
        yield return null;
    }
    
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
