﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class IFallingBox : MonoBehaviour
{
    [SerializeField] private SpriteRenderer sprite;
    
    [SerializeField] protected float fallingSpeed = 1f;
    [SerializeField] protected float fadeSpeed = 1f;
    private WaitForFixedUpdate fu = new WaitForFixedUpdate();
    
    public bool isFalling = false;
    
    public void OnCollisionEnter(Collision col)
    {
    }

    public void Start()
    {
        sprite.DOFade(0, 0);
    }

    public void StartFalling(Vector3 sPos, Vector3 ePos)
    {
        StartCoroutine(IeStartFalling(sPos, ePos));
    }
    
    public IEnumerator IeStartFalling(Vector3 sPos, Vector3 ePos)
    {
        yield return null;
        
        sprite.DOFade(1, 0);
        isFalling = true;

        float progress = 0;
        do
        {
            transform.position = Vector2.Lerp(sPos, ePos, progress);
            progress += fallingSpeed;
            yield return fu;
        } while (this.transform.position.y > ePos.y);
        
        
        sprite.DOFade(0, fadeSpeed)
            .OnComplete(()=> {isFalling = false;});
        
        yield return null;
    }
    
    public void ChangeSpeed(float speed)
    {
        this.fallingSpeed = speed;
    }
    
    public void ChangeSprite(Sprite sprite)
    {
        this.sprite.sprite = sprite;
    }
}
