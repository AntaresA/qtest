﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyBoxesPool : MonoBehaviour
{
    [SerializeField] private FallingBoxEnemy enemy;
    [SerializeField] private List<FallingBoxEnemy> boxes;

    public FallingBoxEnemy GetEnemyBox()
    {
        foreach (var b in boxes)
        {
            if (!b.isFalling)
                return b;
        }

        var newB = Instantiate(enemy);
        boxes.Add(newB);
        return newB;
    }

    public void ChangeFallingSpeed(float nSpeed)
    {
        foreach (var b in boxes)
            b.ChangeSpeed(nSpeed);
        
        enemy.ChangeSpeed(nSpeed);
    }
    
    public int GetPoolSize()
    {
        return boxes.Count;
    }
}