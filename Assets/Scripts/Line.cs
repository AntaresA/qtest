﻿using System;
using UnityEngine;

public class Line : MonoBehaviour
{
    public Action<Collider2D> touch;


    private void OnTriggerEnter2D(Collider2D other)
    {
        touch?.Invoke(other);
    }
}
