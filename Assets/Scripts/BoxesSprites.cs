﻿using UnityEngine;

public class BoxesSprites : MonoBehaviour
{
    [SerializeField] private Sprite[] sprites;

    public Sprite GetRandomEnemyBoxSprite()
    {
        System.Random rnd = new System.Random();
        int r = rnd.Next(0, sprites.Length);

        return sprites[r];
    }
}
